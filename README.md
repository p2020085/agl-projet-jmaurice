# agl-projet-jmaurice
MAURICE Johan
johan.maurice@etu.univ-lyon1.fr

Niveau java : je peux me débrouiller

Projet Kanban : https://aspe-2021-iut-lyon1.bavori.tech/?controller=BoardViewController&action=show&project_id=8&search=
Documentation Kanban : https://docs.kanboard.org/en/1.2.18/user_guide/index.html

Diagramme d'activité : http://yuml.me/edit/4fb44c97

![Diagramme activite](http://yuml.me/4fb44c97.png)

Code source diagramme d'activité : 
(start)->(Se connecter a la forge)->(Saisir MDP)-><c>[mot de passe OK]-><d>[Au moins 1 projet]->(Selectionner Projet)->(Exporter Projet)-|a|
|a|->[Exporter en PDF]->|b|
|a|->[Exporter en HTML]->|b|
|b|->[Export reussi]->(end)
<c>[Mot de passe faux]->(saisir MDP)
<d>[Aucun projet]->(Export impossible)->(end)

Reprise du kanboard : Partie III exo

Info binôme :
Nom : JANUMPORN Malasri
Mail : malasri.janumporn@etu.univ-lyon1.fr
Adresse dépôt Git : https://forge.univ-lyon1.fr/p2008633/agl-projet-mjanumporn
