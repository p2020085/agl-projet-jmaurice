/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.agl.projet;
import lyon1.iutinfo.agl.projet.models.Member;
import lyon1.iutinfo.agl.projet.models.Project;
import lyon1.iut.agl.projet.exception.NegativeNumber;

/**git 
 *
 * @author johan
 */
public class CDVAProjetMain {

    /**
     * @param args the command line arguments
     * @throws lyon1.iut.agl.projet.exception.NegativeNumber
     */
    public static void main(String[] args) throws NegativeNumber {
        Project p1= new Project();
        Member m1 =new Member();
        Member m2 =new Member();
        Member m3 =new Member();
        
        m1.setName("Maurice");
        m2.setName("Sarran");
        m3.setName("Triste");
        
        p1.addMember(m1);
        p1.addMember(m2);
        p1.setID(1);
        p1.setName("AGL");
        p1.setSshURL("aaaa.com");
        p1.setWebURL("ppp.fr");
        
        m1.setId(-1);
        m1.addProject(p1);
        m1.setWebsite("jmaurice.fr");
        m1.setEmail("jmaurice@coucou.fr");
        m2.setId(2);
        m3.setId(2);
        
        System.out.println(p1.toHTML());
        
    }
    
}
