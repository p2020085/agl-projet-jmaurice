/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.agl.projet.models;
import java.util.ArrayList;
import lyon1.iut.agl.projet.exception.NegativeNumber;


/**
 *
 * @author johan
 */
public class Member {
    
    
    /**
     * 
     */
    public Member()
        {
        
        this.lstProject = new ArrayList<Project>();
        
        }
    
    /**
     * 
     * @param v_id member's ID
     * @param v_nom member's Name
     * @param v_email member's Email @
     * @param v_website member's website @
     */
    public Member(int v_id, String v_nom, String v_email, String v_website)
        {
        
        this.id=v_id;
        this.name=v_nom;
        this.email=v_email;
        this.website=v_website;
        
        this.lstProject = new ArrayList<Project>();
        
        }
    
    /**
     * 
     * @return description of object member
     */
    @Override
    public String toString()
        {
        String str;
        str="projet ID #"+this.id+"\nNom : "+this.name
        +"\nEmail : "+this.email
        +"\nWebsite : "+this.website
        +"\nProjet ("+this.lstProject.size()+")\n    "; 
       
        for (Project p : this.lstProject )
            {
           
            str = str+"\t#"+p.getID()+" "+p.getName()+" ("+p.getNbMembers()+")";
           
            }
       
        return str; 
        }
    
    /**
     * 
     * @return ID
     */
    public int getId()   
        {
        
        return this.id;
        
        }
    /**
     * 
     * @return Name
     */
    public String getName()
        {
        
        return this.name;
        
        }
    /**
     * 
     * @return Email
     */
    public String getEmail()
        {
        
        return this.email;
        
        }
    /**
     * 
     * @return Website
     */
    public String getWebsite()
        {
        
        return this.website;
        
        }
    /**
     * 
     * @return project list
     */
    public ArrayList getLstProject()
        {
        
        return this.lstProject;
        
        }
    /**
     * 
     * @param v_id value of new member's ID
     * @throws lyon1.iut.agl.projet.exception.NegativeNumber
     */
    public void setId(int v_id) throws NegativeNumber
        {
        if (v_id<0) throw new NegativeNumber("Number error : Negative Number on input");
        this.id=v_id;
        
        }
    /**
     * 
     * @param v_email value of new member's email @
     */
    public void setEmail(String v_email)
        {
        
        this.email=v_email;
        
        }
    /**
     * 
     * @param v_nom value of new member's name
     */
    public void setName(String v_nom)
        {
        
        this.name=v_nom;
        
        }
    /**
     * 
     * @param v_website value of new member's website @ 
     */
    public void setWebsite(String v_website)
        {
        
        this.website=v_website;
        
        
        }
   
    /**
     * 
     * @param p project to add in lstProject
     */
    public void addProject(Project p)
        {
        
        this.lstProject.add(p);
        
        }
    /**
     * 
     * @return number of project in lstProject
     */
    public int getNbProjets()
        {
        
        return this.lstProject.size();
        
        }
    /**
     * 
     * @return HTML code to display an member object on your lovely browser
     */
    public String toHTML()
        {
        
        String str="<!DOCTYPE html>\n"
        + "<html lang=fr>\n"
        + "<head>\n"
        +"<meta charset='utf-8' />\n"    
        +"<title>"+this.getName()+"</title>\n"
        +"</head>\n"
        +"<body>\n"
        +"<p>Membre ID #"+this.id+"</p>\n"
        +"<p>Name : "+this.name+"</p>\n"
        +"<p>Email : "+this.email+"</p>\n"
        +"<p>Website : "+this.website+"</p>\n"
        +"<p>Number of Project : "+this.getNbProjets()+"</p>\n"
        +"<pre>\n";
       
        for (Project p : this.lstProject)
            {
            str = str+"\t#"+p.getID()+" "+p.getName()+" ("+p.getNbMembers()+")\n"; 
            }
        
       str=str+"</pre>\n"
        +"</body>\n"
        +"</html>"; 
                
        return str; 
        }
    
    protected int id;
    protected String name;
    protected String email;
    protected String website;
    protected ArrayList<Project> lstProject; 
    
}
