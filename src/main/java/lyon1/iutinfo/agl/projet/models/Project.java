/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.agl.projet.models;
import java.util.ArrayList;
import lyon1.iut.agl.projet.exception.NegativeNumber;

/**
 *
 * @author johan
 */
public class Project {
    /**
     * 
     * 
     */
    public Project(){
        
        this.lstMember= new ArrayList<>();
        
    }
    
    /**
     * 
     * @param v_id Project's ID
     * @param v_nom Project's name
     * @param v_web_url Project's url
     * @param v_ssh Project SSH URL
     * @param v_nb number of commit on the project
     */
    public Project(int v_id, String v_nom, String v_web_url, String v_ssh, int v_nb){
        
        
        
        this.id=v_id;
        this.name=v_nom;
        this.webURL=v_web_url;
        this.sshURL=v_ssh;
        this.nbCommits=v_nb;
        this.lstMember= new ArrayList<>();
        
    }
    /**
     * 
     * @return description of object Project
     */
   @Override
   public String toString()
       {
       String str;
       str="projet ID #"+this.id+
       "\nNom : "+this.name+
       "\nsshURL : "+this.sshURL+
       "\nwebURL : "+this.webURL+
       "\nCommits : "+this.nbCommits+
       "\nMembres ("+this.lstMember.size()+")\n";
       
       for (Member m : lstMember)
           {
           
           str = str+"\t#"+m.getId()+" "+m.getName()+" ("+m.getNbProjets()+")\n";
           
           }
       
       return str;
       }
    
   /**
    * 
    * @return project's id
    */
    public int getID()
        {
        
        return this.id;
        
        }
    /**
     * 
     * @return project's name 
     */
    public String getName()
        {
        
        return this.name;
        
        }
    /**
     * 
     * @return  project's URL
     */
    public String getWebURL()
        {
        
        return webURL;
        
        }
    /**
     * 
     * @return project's SSH URL
     */
    public String getSshURL()
        {
        
        return sshURL;
        
        }
    /**
     * 
     * @return project's number of commit
     */
    public int getNbCommits()
        {
        
        return nbCommits;
        
        }
    /**
     * 
     * @param v_id value of new project's ID
     * @throws lyon1.iut.agl.projet.exception.NegativeNumber
     */
    public void setID(int v_id) throws NegativeNumber
        {
        if (v_id<0) throw new NegativeNumber("Number error : Negative Number on input");
        this.id=v_id;
        
        }
    /**
     * 
     * @param v_name value of new project's name
     */
    public void setName(String v_name)
        {
        
        this.name=v_name;
        
        }
    
       public ArrayList<Member> getLstMember() 
            {
            return lstMember;
            }
    
    /**
     * 
     * @param v_web_url value of new project's URL
     */
    public void setWebURL(String v_web_url)
        {
        
        this.webURL=v_web_url;
        
        }
    /**
     * 
     * @param v_ssh value of new project's SSH URL
     */
    public void setSshURL(String v_ssh)
        {
        
        this.sshURL=v_ssh;
        
        }
    /**
     * 
     * @param v_nb_commit value of new project's commit number
     */
    public void setNbCommits(int v_nb_commit)
        {
        
        this.nbCommits=v_nb_commit;
        
        }
    /**
     * 
     * @param new_membre member to add to the project
     */
    public void addMember(Member new_membre)
        {
        
        this.lstMember.add(new_membre);
        
        }
    /**
     * 
     * @param lst_member set a new ArrayList of lst_member
     */
    public void setLstMembres(ArrayList lst_member)
        {
        
        this.lstMember = lst_member;
        
        }
    /**
     * 
     * @return number of member affected on the project
     */
    public int getNbMembers()  
        {
        
        return lstMember.size();
        
        }
    /**
     * 
     * @return HTML code to display on your favorite browser
     */
    public String toHTML()
        {
        
        String str;
        
        
        
       str="<!DOCTYPE html>\n"
        + "<html lang=fr>\n"
        + "<head>\n"
        +"<meta charset='utf-8' />\n"    
        +"<title>"+this.getName()+"</title>\n"
        +"</head>\n"

        +"<body>\n"
        +"<p>projet ID #"+this.id+"</p>\n"
        +"<p>Nom : "+this.name+"</p>\n"
        +"<p>sshURL : "+this.sshURL+"</p>\n"
        +"<p>webURL : "+this.webURL+"</p>\n"
        +"<p>Commits : "+this.nbCommits+"</p>\n"
        +"<p>Membres ("+this.lstMember.size()+")<br>";
       
        for (Member m : this.lstMember)
            {
           
            str = str+"\t#"+m.getId()+" "+m.getName()+" ("+m.getNbProjets()+")<br>";
           
            }
       
        str=str+"</p>\n"+
        "</body>\n"+
        "</html>"; 
                
        return str; 
        
        
        }
    
    protected int id;
    protected String name;
    protected String webURL;
    protected String sshURL;
    protected int nbCommits;
    protected ArrayList<Member> lstMember;

    
}
