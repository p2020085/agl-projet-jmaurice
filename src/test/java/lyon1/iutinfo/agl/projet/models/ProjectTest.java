/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.agl.projet.models;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;
import java.io.File;
import java.io.IOException;
import java.io.*;
import lyon1.iut.agl.projet.exception.NegativeNumber;

/**
 *
 * @author malas
 */
public class ProjectTest {
    
    public ProjectTest() 
        {
        }
    
    @BeforeAll
    public static void setUpClass() 
        {
        }
    
    @AfterAll
    public static void tearDownClass() 
        {
        }
    
    @BeforeEach
    public void setUp() 
        {
        }
    
    @AfterEach
    public void tearDown() 
        {
        }
   
  
        public String getDigest(String f) 
            {
            //String f = "output/Projet_Control.html";
            try 
                {
                return new DigestUtils(SHA_224).digestAsHex(new File(f));
                } 
            catch (IOException e)
                {
                e.printStackTrace(System.err);
                }
            return "";
            }    
        
      /**
     * Test toString, class Projet
     */
    @Test
    public void testToString() 
        {
        System.out.println("toString");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        String str="projet ID #"+p.id+
        "\nNom : "+p.name+
        "\nsshURL : "+p.sshURL+
        "\nwebURL : "+p.webURL+
        "\nCommits : "+p.nbCommits+
        "\nMembres ("+p.lstMember.size()+")\n";
       
       for (Member m : p.lstMember)
            {
           
            str = str+"\t#"+m.getId()+" "+m.getName()+" ("+m.getNbProjets()+")\n";
           
            }

        String result = p.toString();
        assertEquals(str, result);
        
        System.out.println("Done. ");
    
    }
     /**
     * Test getId, class Project.
     */
    @Test
    public void testGetId() 
        {
        System.out.println("getId");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        int expResult = 1;
        int result = p.getID();
        assertEquals(expResult, result);
        
        System.out.println("Done. ");
        }
     /**
     * Test getName, class Projet.
     */
    @Test
    public void testGetName() 
        {
        System.out.println("getNom");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        String expResult = "TEST";
        String result = p.getName();
        assertEquals(expResult, result);
        
         System.out.println("Done. ");   
        }
    
     /**
     * Test getWebURL, class Projet.
     */
    @Test
    public void testGetWebURL() 
        {
        System.out.println("getWebURL");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        String expResult = "test.fr";
        String result = p.getWebURL();
        assertEquals(expResult, result);
       
        System.out.println("Done. ");   
        }
    
     /**
     * Test getSshURL, class Projet.
     */
    @Test
    public void testGetSshURL() 
        {
        System.out.println("getSshURL");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        String expResult = "sshtest.fr";
        String result = p.getSshURL();
        assertEquals(expResult, result);
        
        System.out.println("Done. ");   
        }
    
    /**
     * Test getNbCommits , class Projet.
     */
    @Test
    public void testGetNbCommits() 
        {
        System.out.println("getNbCommits");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",2);
        int expResult = 2;
        int result = p.getNbCommits();
        assertEquals(expResult, result);
        
        System.out.println("Done. ");   
        }
    
     /**
     * Test getLstMembres , class Projet.
     */
    @Test
    public void testGetLstMember() 
        {
        System.out.println("getLstMembres");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        
        ArrayList expResult = p.getLstMember();
        ArrayList result = p.getLstMember();
        assertEquals(expResult, result);
     
        System.out.println("Done. ");   
        }
    
    /**
    * Test setID, class Projet 
     * @throws lyon1.iut.agl.projet.exception.NegativeNumber
    */
    @Test
    public void testSetID() throws NegativeNumber
        {
        System.out.println("setID");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        int expResult=2;
        p.setID(expResult);
        assertEquals(expResult,p.getID());
        
        System.out.println("Done. ");   
        }
    
    /**
     * Test setName, class Projet.
     */
    @Test
    public void testSetName() 
        {
        System.out.println("setName");
        String name = "TEST2";
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        p.setName(name);
        assertEquals(name, p.getName());
        
        System.out.println("Done. ");   
        }
    
    /**
     * Test setWebURL, class Projet
     */
    @Test
    public void testSetWebURL()
        {
        System.out.println("setWebURL");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        String expResult = "test2.fr";
        p.setWebURL(expResult);
        String result = p.getWebURL();
        assertEquals(expResult,result);
        
        System.out.println("Done. ");   
        }
    /**
     * Test setSshURL, class Projet
     */
    @Test
    public void testSetSshURL()
        {
        System.out.println("setWebURL");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        String expResult = "sshtest2.fr";
        p.setSshURL(expResult);
        String result = p.getSshURL();
        assertEquals(expResult,result);
        
        System.out.println("Done. ");   
        }
    
    /**
     * Test of setNbCommits method, of class Projet.
     */
    @Test
    public void testSetNbCommits() 
        {
        System.out.println("setNbCommits");
        int nbCommits = 1;
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        p.setNbCommits(nbCommits);
        assertEquals(nbCommits, p.getNbCommits());
    
        System.out.println("Done. ");   
        }
    /**
     * Test addMembre, class Projet.
     */
    @Test
    public void testAddMembre() 
        {
        System.out.println("addMembre");
        Member m = new Member(1,"Johan","johan@gmail.com","johanwebsite.fr");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        assertEquals(0, p.lstMember.size()); 
        p.addMember(m);
        assertEquals(1, p.lstMember.size()); 
        
        System.out.println("Done. ");   
        }

    
   /**
     * Test setLstMembres, class Projet
     */
    @Test
    public void testSetLstMembres()
        {
        System.out.println("setLstMembre");
        Member m = new Member(1,"Johan","johan@gmail.com","johanwebsite.fr");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);

        ArrayList<Member> expResult = new ArrayList<>();
        expResult.add(m);
        p.setLstMembres(expResult);
        ArrayList<Member> result = p.lstMember;
        assertEquals(expResult,result);
        
        System.out.println("Done. ");  
        }

     /**
     * TestbgetNbMembres, class Projet.
     */
    @Test
    public void testGetNbMembres() 
        {
        System.out.println("getNbMembres");
        Member m = new Member(1,"Johan","johan@gmail.com","johanwebsite.fr");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        int expResult = 0;
        int result = p.getNbMembers();
        assertEquals(expResult, result);
        p.addMember(m);
        expResult = 1;
        result = p.getNbMembers();
        assertEquals(expResult, result);
        
        System.out.println("Done. ");  
        }
    

    /**
     * Test toHTML, Projet.
     * @throws java.io.IOException*/
    @Test
    public void testToHTML() throws IOException 
        {
        System.out.println("toHTML");
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        try (PrintWriter sortie = new PrintWriter( new FileWriter("output/projet_test.html") ))
        {
          sortie.print(p.toHTML());
        }
        Project p2 = new Project(1, "TEST", "test.fr","sshtest.fr",0); 
        assertEquals(p.toHTML(),p2.toHTML());
        //assertEquals(this.getDigest("output/projet_control.html"),this.getDigest("output/projet_test.html"));
        
        System.out.println("Done. ");  
        
        }

}
    
       
    
