/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.agl.projet.models;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lyon1.iut.agl.projet.exception.NegativeNumber;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;


/**
 *
 * @author johan
 */
public class MemberTest {
    
     public class DemoDigest {
        
        public String getDigest(String s, String s1) 
            {
            String f = s1;
            try 
                {
                return new DigestUtils(SHA_224).digestAsHex(new File(f));
                } catch (IOException ex) 
                    {
                    Logger.getLogger(MemberTest.class.getName()).log(Level.SEVERE, null, ex);
                    }
            return "";
            }
       
    }
    
    public MemberTest() 
        {
        }
    
    @BeforeAll
    public static void setUpClass() 
        {
        }
    
    @AfterAll
    public static void tearDownClass() 
        {
        }
    
    @BeforeEach
    public void setUp() 
        {
        }
    
    @AfterEach
    public void tearDown() 
        {
        }

    /**
     * Test of toString method, of class Member.
     */
    @Test
    public void testToString() 
        {
        System.out.println("toString");
        Member m = new Member(1,"Johan","j@hotmail.fr","j.fr");
        String str;
        str="projet ID #"+m.id+"\nNom : "+m.name
        +"\nEmail : "+m.email
        +"\nWebsite : "+m.website
        +"\nProjet ("+m.lstProject.size()+")\n    "; 
       
       for (Project p : m.lstProject )
            {
           
            str = str+"\t#"+p.getID()+" "+p.getName()+" ("+p.getNbMembers()+")";
           
            }
       
        String result = m.toString();
        assertEquals(str, result);
        
        }

    /**
     * Test of getId method, of class Member.
     */
    @Test
    public void testGetId() 
        {
            
        System.out.println("getId");
        Member instance = new Member(1,"Johan","j@hotmail.fr","j.fr");
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
        
        }

    /**
     * Test of getNom method, of class Member.
     */
    @Test
    public void testGetName() 
        {
            
        System.out.println("getNom");
        Member m = new Member(1,"Johan","j@hotmail.fr","j.fr");
        String expResult = "Johan";
        String result = m.getName();
        assertEquals(expResult, result);
       
        }

    /**
     * Test of getEmail method, of class Member.
     */
    @Test
    public void testGetEmail() 
        {
            
        System.out.println("getEmail");
        Member m = new Member(1,"Johan","j.m@gmail.com","j.fr");
        String expResult = "j.m@gmail.com";
        String result = m.getEmail();
        assertEquals(expResult, result);
       
        }

    /**
     * Test of getWebsite method, of class Member.
     */
    @Test
    public void testGetWebsite() 
        {
            
        System.out.println("getWebsite");
        Member m = new Member(1,"Johan","j.m@gmail.com","j.fr");
        String expResult = "j.fr";
        String result = m.getWebsite();
        assertEquals(expResult, result);
       
        }

    /**
     * Test of getLstProject method, of class Member.
     */
    @Test
    public void testGetLstProject() 
        {
            
        System.out.println("getLstProjets");
        Member m = new Member(1,"Johan","j.m@gmail.com","j.fr");
        ArrayList expResult = m.lstProject;
        ArrayList result = m.getLstProject();
        assertEquals(expResult, result);
        
        }

    /**
     * Test of setId method, of class Member.
     * @throws lyon1.iut.agl.projet.exception.NegativeNumber
     */
    @Test
    public void testSetId() throws NegativeNumber 
        {
            
        System.out.println("setId");
        Member m = new Member(1,"Johan","j.m@gmail.com","j.fr");
        int vId =10;
        m.setId(vId);
        assertEquals(10,m.getId());
        
        }

    /**
     * Test of setEmail method, of class Member.
     */
    @Test
    public void testSetEmail() 
        {
            
        System.out.println("setEmail");
        String vEmail = "coucou@hotmail.fr";
        Member instance = new Member(1,"Johan","j.m@gmail.com","j.fr");
        instance.setEmail(vEmail);
        assertEquals(vEmail, instance.getEmail());
        
        }

    /**
     * Test of setNom method, of class Member.
     */
    @Test
    public void testSetName() 
        {
            
        System.out.println("setNom");
        String vNom = "Jean";
        Member m = new Member(1,"Johan","j.m@gmail.com","j.fr");
        m.setName(vNom);
        assertEquals(vNom,m.getName());
       
        }

    /**
     * Test of setWebsite method, of class Member.
     */
    @Test
    public void testSetWebsite() 
        {
            
        System.out.println("setWebsite");
        String vWebsite = "https://j.m.fr";
        Member m = new Member(1,"Johan","j.m@gmail.com","j.fr");
        m.setWebsite(vWebsite);
        assertEquals(vWebsite, m.getWebsite());
        
        }


    /**
     * Test of addProjet method, of class Member.
     */
    @Test
    public void testAddProject() 
        {
            
        System.out.println("addProjet");
        //create a member
        Member m = new Member(1,"Johan","johan@gmail.com","johanwebsite.fr");
        //create a project
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        //test if getNbProject send the good number of project
        assertEquals(0,m.getNbProjets());
        m.addProject(p);
        //test if getNbProject send the good number of project after adding a project
        assertEquals(1,m.getNbProjets());
       
        }

    /**
     * Test of getNbProjets method, of class Member.
     */
    @Test
    public void testGetNbProject() 
        {
            
        System.out.println("getNbProjets");
        //create a membre
        Member m = new Member(1,"Johan","johan@gmail.com","johanwebsite.fr");
        //create a project
        Project p = new Project(1, "TEST", "test.fr","sshtest.fr",0);
        //test if a member without project send 0 project.
        assertEquals(0,m.getNbProjets());
        m.addProject(p);
        //test if when you had a project you have +1.
        assertEquals(1,m.getNbProjets());
        }

    /**
     * Test of toHTML method, of class Member.
     */
    @Test
    public void testToHTML() 
        {
        
        System.out.println("toHTML");
        Member m = new Member(1,"Johan","johan@gmail.com","johanwebsite.fr");
        
        DemoDigest d1= new DemoDigest();
        //test if two files have the same HASHCODE
        assertEquals(d1.getDigest(SHA_224, "output/project_control.html"),d1.getDigest(SHA_224, "output/project_test.html"));
        
        }
    
}
